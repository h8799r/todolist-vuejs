import { reactive } from 'vue';
import { Todo, TodoList, TodoStatus } from '@/model/todo';
import { v4 as uuidv4 } from 'uuid';
import { create, get } from '@/service/api';


const defaultList = new TodoList([]);

export const store = reactive({
  todoList: defaultList,
  isDone(id: string) {
    this.todoList.isDone(id);
  },
  add(content: string) {
    const newUUID = uuidv4();
    const todo = new Todo(newUUID, content, TodoStatus.TODO);
    create(todo);
    this.todoList.add(todo);
  },
  remove(id: string) {
    // delete(id);
    this.todoList.remove(id);
  },
  updateStatus(id: string, status: TodoStatus) {
    this.todoList.setStatus(id, status);
  },
  async loadAllTodos() {
    await get().then((response: Todo[]) => {
      console.log(`Store response = ${response}`);
      for(let todo of response) {
        console.log(todo.content);
      }
      this.todoList.resetList(response);
    });
  }
});
